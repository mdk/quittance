#!/usr/bin/env python3

import datetime as dt
import json
import locale
import sys
from argparse import ArgumentParser
from pathlib import Path

import tomlkit as toml
from jinja2 import Environment, PackageLoader, select_autoescape
from weasyprint import HTML


def parse_args():
    parser = ArgumentParser(description="Génère une quittance de loyer")
    parser.add_argument("--date")
    parser.add_argument(
        "--debug", "-d", help="Keep HTML file for debug purposes.", action="store_true"
    )
    subparsers = parser.add_subparsers(required=False)

    parser_conf = subparsers.add_parser(
        "config",
        help="Quittance configuration. Run without argument to see the current configuration.",
    )
    parser_conf.add_argument("--bailleur-name", metavar="'Ano Nymous'")
    parser_conf.add_argument("--bailleur-address", metavar="'10 route de la corniche'")
    parser_conf.add_argument("--bailleur-code-postal", metavar="69000")
    parser_conf.add_argument("--bailleur-city", metavar="'Lyon'")
    parser_conf.add_argument("--bailleur-country", metavar="'France'")
    parser_conf.add_argument(
        "--bailleur-signature", metavar="/home/user/Documents/signature.png"
    )
    parser_conf.add_argument("--locataire-name", metavar="'Ano Nymous'")
    parser_conf.add_argument("--locataire-address", metavar="'10 route de la corniche'")
    parser_conf.add_argument("--locataire-code-postal", metavar="'69000'")
    parser_conf.add_argument("--locataire-city", metavar="'Lyon'")
    parser_conf.add_argument("--locataire-country", metavar="'France'")
    parser_conf.add_argument("--dernier-numero", type=int, metavar="123")
    parser_conf.add_argument("--loyer-hors-charges", type=int, metavar="123")
    parser_conf.add_argument("--charges", type=int, metavar="123")
    parser_conf.set_defaults(func=main_config)

    parser.set_defaults(func=main_new)

    return parser.parse_args()


def main():
    args = parse_args()
    func = args.func
    del args.func
    return func(args)


def load_config():
    try:
        with open(Path.home() / ".config" / "quittance.toml", "r") as f:
            config = toml.load(f)
    except FileNotFoundError:
        config = {}
    return config


def save_config(config):
    with open(Path.home() / ".config" / "quittance.toml", "w") as f:
        toml.dump(config, f)


def main_config(args):
    keys = set(vars(args).keys())
    args = {key: value for key, value in vars(args).items() if value is not None}
    config = load_config()
    config.update(args)
    if "dernier_numero" not in config:
        config["dernier_numero"] = 0
    for missing_key in sorted(keys - set(config)):
        config[missing_key] = ""
    save_config(config)
    print(
        f"You can edit the configuration file: {Path.home() / '.config' / 'quittance.toml'}"
    )
    print(toml.dumps(config))


def get_month(date):
    first_of_month = date.replace(day=1)
    next_month = first_of_month + dt.timedelta(days=32)
    first_day_of_next_month = next_month.replace(day=1)
    last_of_month = first_day_of_next_month - dt.timedelta(days=1)
    return f"du {first_of_month:%A %d %B %Y} au {last_of_month:%A %d %B %Y}"


def main_new(args):
    locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")
    env = Environment(loader=PackageLoader("quittance"), autoescape=select_autoescape())
    template = env.get_template("quittance.html")
    config = load_config()

    env = config.copy()

    if args.date is None:
        date = dt.date.today()
    else:
        date = dt.datetime.strptime(args.date, "%Y-%m-%d").date()

    env["date"] = date.strftime("%A %d %B %Y")
    env["periode"] = get_month(date)

    config["dernier_numero"] += 1
    env["quittance_no"] = config["dernier_numero"]
    save_config(config)

    output = f"{date:%Y-%m-%d} quittance-{env['quittance_no']}.pdf"
    html = template.render(**env)
    if args.debug:
        Path("quittance.html").write_text(html, encoding="UTF-8")
        print("[debug] You can inspect the 'quittance.html' file.")
    HTML(string=html, base_url=str(Path(template.filename).parent)).write_pdf(output)
    print("Nouvelle quittance :", output)


if __name__ == "__main__":
    main()
